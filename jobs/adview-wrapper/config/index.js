'use strict';

const { PORT = 3000, PUBLISHER } = process.env;

module.exports = {
  PORT,
  PUBLISHER,
  URL: 'https://adview.online/api/v1/jobs.json',
  WHITELIST: [
    `http://localhost:8080`,
    `http://127.0.0.1:8080`,
    `http://localhost:8081`,
    `http://localhost:8082`,
    `http://localhost:${process.env.PORT}`,
    'https://zsolti.co',
    'https://zsolti.me',
  ],
};
