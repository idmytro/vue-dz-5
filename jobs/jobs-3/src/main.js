import Vue from 'vue';
import App from './App.vue';
import smoothscroll from 'smoothscroll-polyfill';

Vue.filter('capitalize', (value) => {
  if (value !== value.toUpperCase()) {
    return value.toLowerCase()
      .replace(/\b\w/g, x => x.toUpperCase());
  }
  return value;
});

/* eslint-disable-next-line */
new Vue({
  el: '#app',
  render: h => h(App)
  // template: `<App/>`
});

smoothscroll.polyfill();
