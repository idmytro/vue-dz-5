import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import { f } from './second'
import str from './second'

f()
console.log(str);

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
